
from IPython.display import clear_output

rows = 'ABC'
cols = '123'
symbols = {}
win = {1: 0, 2: 0}
grid = {}
player = 0
full = 0
for rc in [r + c for r in rows for c in cols]:
    grid[rc] = ' '
diag1 = [rows[i] + cols[i] for i in xrange(0, 3)]
diag2 = [rows[i] + cols[2 - i] for i in xrange(0, 3)]


def reset():
    global grid, full
    for rc in [r + c for r in rows for c in cols]:
        grid[rc] = ' '
    full = 0


def init():
    global symbols, player
    print 'Welcome to Tic Tac Toe!'
    symbols = {1: ' ', 2: ' '}
    while len(symbols[1]) != 1 or symbols[1] == ' ':
        symbols[1] = raw_input('Insert symbol for player 1 (1 character): ')
        if len(symbols[1]) != 1 or symbols[1] == ' ':
            print 'Invalid symbol!'
    while len(symbols[2]) != 1 or symbols[2] == symbols[1] or symbols[2] == ' ':
        symbols[2] = raw_input('Insert symbol for player 2 (1 character): ')
        if len(symbols[2]) != 1 or symbols[2] == symbols[1] or symbols[2] == ' ':
            print 'Invalid symbol!'
    player = 0
    while player not in [1, 2]:
        player = raw_input('Select first player (1 or 2): ')
        if not player.isdigit():
            print 'Invalid player!'
        player = int(player)
        if player not in [1, 2]:
            print 'Invalid player!'


def print_grid():
    clear_output()
    header = ' '
    for c in cols:
        header += '  ' + c + ' '
    print header
    separator = ' ' + '+---' * 3 + '+'
    print separator
    empty_row = ' ' + '|   ' * 3 + '|'
    for r in rows:
        row = r + ''
        for c in cols:
            row += '| ' + grid[r + c] + ' '
        print row + '|'
        print separator


def has_win(cell):
    for rc in [cell[0] + c for c in cols]:
        if grid[cell] != grid[rc]:
            break
    else:
        return True
    for rc in [r + cell[1] for r in rows]:
        if grid[cell] != grid[rc]:
            break
    else:
        return True
    if cell in diag1:
        for rc in diag1:
            if grid[cell] != grid[rc]:
                break
        else:
            return True
    if cell in diag2:
        for rc in diag2:
            if grid[cell] != grid[rc]:
                break
        else:
            return True
    return False


def insert_value():
    global player, full
    rc = ''
    while rc not in grid or grid[rc] != ' ':
        rc = raw_input('Player {p} select cell: '.format(p=player))
        if rc not in grid:
            print 'Invalid cell'
        elif grid[rc] != ' ':
            print 'Cell is already full'
    else:
        grid[rc] = symbols[player];
        ex = False
        if has_win(rc):
            win[player] += 1
            print 'Player {p} has win!!! ({w1} - {w2})'.format(p=player, w1=win[1], w2=win[2])
            ex = True
        else:
            full += 1
            if full == 9:
                print 'The game is finished with a draw... ({w1} - {w2})'.format(w1=win[1], w2=win[2])
                ex = True
        player = 2 if player == 1 else 1
        return ex


init()
qu = ' '
while qu != 'q':
    reset()
    print_grid()
    while not insert_value():
        print_grid()
    print_grid()
    qu = raw_input('Press any key to continue (q for quit)')
