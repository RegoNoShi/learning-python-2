import os, time

etl_dir = './output etl/'
etl_file = 'ag3.txt'
new_dir = './output new/'
new_file = 'ag3.txt'

today = time.strftime("%Y%m%d")

sort_file = open('ag3_new_sorted_' + today + '.txt', 'w')
diff_file = open('ag3_new_diff_' + today + '.txt', 'w')

etl = {}
etl_count = 0
duplicated_etl = 0
duplicated_etl_list = []
for line in open(os.path.join(etl_dir, etl_file)):
    key = line[:8].strip()
    if key in etl:
        duplicated_etl += 1
        if key not in duplicated_etl_list:
            duplicated_etl_list.append(key)
    else:
        etl_count += 1
        etl[key] = (line, etl_count)
new = {}
new_count = 0
duplicated_new = 0
duplicated_new_list = []
for line in open(os.path.join(new_dir, new_file)):
    key = line[:8].strip()
    if key in new:
        duplicated_new += 1
        if key not in duplicated_new_list:
            duplicated_new_list.append(key)
    else:
        new_count += 1
        new[key] = (line, new_count)
if duplicated_etl > 0 or duplicated_new > 0:
    diff_file.write('Error, found duplicated keys (etl: {etl}, new: {new})\n'.format(etl=duplicated_etl, new=duplicated_new))
    for key in duplicated_etl_list:
        diff_file.write('    Duplicated key {s} in etl file\n'.format(s=key))
    for key in duplicated_new_list:
        diff_file.write('    Duplicated key {s} in new file\n'.format(s=key))
found = 0
correct = 0
wrong = 0
missing = 0
wrong_list = []
missing_list = []
for key, tup in etl.iteritems():
    if key in new:
        tup2 = new.pop(key)
        found += 1
        if tup[0] == tup2[0]:
            correct += 1
        else:
            sort_file.write(tup[0])
            sort_file.write(tup2[0])
            sort_file.write('-' * max(len(tup[0].strip('\n')), len(tup2[0].strip('\n'))) + '\n')
            wrong_list.append((tup[1], tup2[1]))
            wrong += 1
    else:
        missing_list.append(key)
        missing += 1
diff_file.write('New file contains {n} lines and etl file contains {e} lines\n'.format(n=new_count,e=etl_count))
if found < new_count:
    diff_file.write('New file contains {a} lines not present in etl file\n'.format(a=new_count - found))
    for key, tup in new.iteritems():
        diff_file.write('    Additional key {s} in new file\n'.format(s=key))
diff_file.write('New file contains {c} correct lines\n'.format(c=correct))
if missing > 0:
    diff_file.write('Etl file contains {m} lines not present in new file\n'.format(m=missing))
    for key in missing_list:
        diff_file.write('    Missing key {s} in new file\n'.format(s=key))
diff_file.write('New file contains {w} wrong lines\n'.format(w=wrong))
for tup in wrong_list:
    diff_file.write('    Row {n} of new file is different from row {e} in etl file\n'.format(n=tup[1], e=tup[0]))

sort_file.close()
diff_file.close()
